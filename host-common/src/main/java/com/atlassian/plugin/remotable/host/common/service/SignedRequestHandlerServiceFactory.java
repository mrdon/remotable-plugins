package com.atlassian.plugin.remotable.host.common.service;

import com.atlassian.plugin.remotable.api.service.SignedRequestHandler;

public interface SignedRequestHandlerServiceFactory extends TypedServiceFactory<SignedRequestHandler>
{
}
