package com.atlassian.plugin.remotable.host.common.descriptor;

public interface LocalMountBaseUrlResolver
{
    String getLocalMountBaseUrl(String appKey);
}
