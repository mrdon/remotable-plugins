package com.atlassian.plugin.remotable.test.webhook;

public interface WebHookBody
{
    String find(String expression) throws Exception;
}
