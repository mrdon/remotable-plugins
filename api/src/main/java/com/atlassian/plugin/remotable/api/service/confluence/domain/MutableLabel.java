package com.atlassian.plugin.remotable.api.service.confluence.domain;

/**
 */
public interface MutableLabel
{
    void setId(long id);

    long getId();
}
