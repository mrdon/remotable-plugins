package ao.model;

import net.java.ao.Entity;

public interface SampleEntity extends Entity
{
    void setText(String text);

    String getText();
}
